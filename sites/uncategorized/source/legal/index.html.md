---
layout: markdown_page
title: "GitLab Legal"
---
## Legal Information

### Privacy
- [Privacy Policy](https://about.gitlab.com/privacy/) 
- [Privacy Compliance](https://about.gitlab.com/privacy/privacy-compliance/)

### Terms
- [Website Terms of Use](https://about.gitlab.com/terms/#gitlab-com)
- [Subscription Terms](https://about.gitlab.com/terms/#subscription)
- [Professional Services Terms](https://about.gitlab.com/terms/#consultancy)

### Policies
- [DMCA](https://about.gitlab.com/handbook/dmca/)
- [Trademark Use](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/brand-guidelines/#trademark)
- [Code of Business Conduct](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/)

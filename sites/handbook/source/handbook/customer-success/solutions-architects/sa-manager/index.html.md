---
layout: handbook-page-toc
title: SA Manager Operating Rhythm 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}



View the [SA Handbook homepage](/handbook/customer-success/solutions-architects/) for additional SA-related handbook pages.

---

## Purpose

This page provides an overview on relevant SA Managers processes. The SA Manager role is made up of pillars: Managing the Business and Managing the Team. 

## Managing the Business 

### Activity Review 
As part of the regular rhythm of business, it is important to review activities related to Prospect and Customer Engagements. The expectation is that SA Managers will comment weekly on SA activities in the #troops slack channel(s). Be sure to subscribe to #troops Channel in Slack.

Reviewing Chorus calls at least quarterly can allow for SA Managers to check in and give managers opportunities to provide timely feedback on technical discovery calls or demos. 

### Deliberate Practice 
Deliberate practice allows the team to sharpen their skills and continue delivering deal-winning engagements. The SA Manager needs to support ongoing role play exercises and dry-runs for key accounts. This includes facilitating sessions, helping the team make this a priority, and ensure the team is adhering to key delivery methodologies, including MEDDPPICC and Demo2Win. Important tools include Google Slides, Role Plays, and the [wheel of Names](http://weelofnames.com/).

### Regular Syncs with ASM  
Meeting regularly with your ASM counterpart provides time to discuss SAL/ SA/ TAM Team health, review strategic territory deals, and review opportunity close analysis and regional data. It is recommended to use a combination of SFDC Opportunity Records, Sisense Dashboard Data, and to take notes in Google Docs. Every meeting should have an agenda. 

### Quarterly Business Reviews
The SA Manager is responsible for developing and delivering a [Quarterly Business Review](/handbook/sales/qbrs/) for their team each quarter.

SA Managers typically cover the following information:
- Review past quarter 
- Assess current quarter 
- Review strategic inititaitves
- Review sales talent needs
- Review retrospective results 

## Managing the Team 

### Cognitive Bias Sessions 

It is important to pland and schedule [Coginitive Bias](/handbook/customer-success/solutions-architects/sa-practices/recognizing-cognitive-bias/) Sessions for the team quarterly. Be sure to think about assigning a facilitator and role players, as needed. 

### Identifying and Recruiting
SA Managers should always be recruiting! It is important that SA Managers always be on the lookout for talent and be conscientious in building a talent network. Make time to interview prospective candidates and be able to artiulate the value of a job at GitLab as outlined in the [Winning Top Talent](/handbook/sales/field-manager-development/#winning-top-talent) portion of the GitLab Field Manager Development Program.  

**Key Resources**    
- Success Profile
- Interview Guide
- Networking tools
- "Why GitLab" Summary

### Onboarding
SA Managers should ensure the new SA team members are set up for success by working to make sure they have access to necessary tools and have an assigned onboarding buddy. Additionally, they should work with new SAs to set clear performance expectations and provide regular feedback during the onboarding process. SA Managers should encourage new SAs to complete their general GitLab onboarding issue and ensure that they actively participate in and complete [Sales Quick Start](/handbook/sales/onboarding/).


### Developing and Retaining
Developing and retaining key talent is constantly top-of-mind for high-performing SA Managers. The best SA Managers provide regular [coaching](/handbook/leadership/coaching/), follow [Performance Management](/handbook/sales/field-manager-development/#performance-management-and-giving-feedback) best practices, and leverage GitLab's [Field Functional Competencies](/handbook/sales/training/field-functional-competencies/) to reinforce the critical skills and behaviors that will lead to desired outcomes/results.

Additionally, SA Managers should reinforce [key enablement initiatives](https://gitlab.edcast.com/teams/solution-architects-sas) and encourage participation in the [CS Skills Exchange](/handbook/sales/training/customer-success-skills-exchange/). 

#### One-on-Ones
As part of reviewing and supporting the team activities, SA Managers should meet with each team member weekly. This is imporant to build rapport and trust between manager and team member. With this is mind, SA Managers should add topics to the agenda and encourage team members to contributre to the agenda as well. One-on-Ones can be a great place to check on team member's health and workload, identify needs of individual team members, review top deals, and provide feedback and coaching. 

### Team Assessment
Twice a year, SA Managers should conduct a team assessment to: 
-  Identify at Risk and High Potential Team Members
- Identify resource needs for the next 6-months 
- Determine succession plan  
- Update rolling 6 to 12 month promotion forecast 

---
layout: handbook-page-toc
title: "Employee Privacy Policy"
description: "This Employee Privacy Policy explains what types of personal information we may collect about our employees and how it may be used"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Employee Privacy Policy

This Employee Privacy Policy (“Privacy Policy”) explains what types of personal information we may collect about our employees and how it may be used.

While this Privacy Policy is intended to describe the broadest range of our information processing activities globally, those processing activities may be more limited in some jurisdictions based on the restrictions of their laws. For example, the laws of a country may limit the types of personal information we can collect or the manner in which we process that information. In those instances, we adjust our internal policies and practices to reflect the requirements of local law.

For personal data collected under this Privacy Policy, the controller will be GitLab and the GitLab affiliates by which you are employed. For, (i) specific security concerns around your data, (ii) in the event you feel that you hve not received proper attention to your data request, or (iii) have any other data privacy concerns, please contact GitLab Legal by emailing [legal_internal@gitlab.com](mailto:legal_internal@gitlab.com). 


GitLab, Inc. is a global company with its headquarters in the U.S. This means that personal information may be used, processed, and transferred to the United States and other countries or territories and those countries or territories may not offer the same level of data protection as the country where you reside, including the European Economic Area.  However, GitLab will ensure that appropriate or suitable safeguards are in place to protect your personal information and that transfer of your personal information complies with applicable data protection laws. Where required by applicable data protection laws, GitLab has ensured that service providers (including other GitLab affiliates) sign standard contractual clauses as approved by the European Commission or other supervisory authority with jurisdiction over the relevant GitLab data exporter (which typically will be your employer).

*Who is collecting your personal data (who is the data controller)?*

The GitLab entity that is a party to your employment contract or contract for services or otherwise employs you will be the data controller of your personal data.  The following are the GitLab entities that act as controller:  GitLab, Inc., GitLab, LLC., GitLab BV, GitLab GK, GitLab GmbH, GitLab PTY Ltd, GitLab Canada Corp, GitLab IT BV, GitLab UK, Ltd., GitLab Ireland Ltd., GitLab Korea Limited, GitLab Singapore Pte Ltd., GitLab France S.A.S. and other GitLab subsidiaries throughout the globe (collectively "GitLab").

GitLab affiliates may act as processors on behalf of other GitLab affiliates and/ or controllers.  Furthermore, GitLab, its affiliates and subsidiaries participate in a group-wide IT system in order to harmonize GitLab’s IT infrastructure and its use (the “System”). The System also may hold data on all employees, workers, individual contractors and contingent workers ("Staff"). Insofar the System serves to improve and harmonize most of the human resources (“HR”) processes within GitLab. GitLab, Inc. in the U.S. is responsible for the System.

*Applicability of Other GitLab Privacy Policies*

The websites of GitLab (e.g., [about.gitlab.com](/)) have separate privacy policies and terms of use that apply to their use. Additionally, some of our third party products and services may have separate privacy policies and terms of use that apply to their use. Any personal information collected in connection with your use of those websites or products and services are not subject to this Privacy Policy.  If you are unsure how or if this Privacy Policy applies to you, please contact your DPO or Privacy Officer.

*Third Party Services*

In some cases, you may provide personal information to third parties that GitLab works with or that provide services to GitLab.  This includes, those parties identified in the [Tech Stack Application YAML](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml) (“Third Parties”).

The Tech Stack is updated periodically to ensure accurate, up-to-date disclosure of employee and customer third party applications used at GitLab.  This particular policy applies to those applications identified as relating to Employee applications.  The use of such Third Party websites may be governed by separate terms of use and privacy policies which are not under our control and are not subject to this Privacy Policy. Please contact such Third Parties for questions regarding their privacy practices, as well as if you would like to have them modify, update, alter or delete your personal information.  Please understand that there are exceptions to rights surrounding personal data relating to employment.  GitLab is required to maintain certain employment information by law.

*What is Personal Information?*

Personal information, also known as personally identifiable information or personal data, for purposes of this Privacy Policy means any information that (i) directly and clearly identifies an individual, or (ii) can be used in combination with other information to identify an individual. Personal information does not include such information if it is anonymous or if it has been rendered de-identified by removing personal identifiers.

Examples of personal information include:
 - An individual’s name.
 - Employee ID number.
 - Home address.
 - Home phone number.
 - Personal email address.
 - Names of family members.
 - Date of birth.

*What is Sensitive Personal Information?*

Sensitive personal information is a subset of personal information that may be more sensitive in nature for the individual concerned.

Examples of sensitive personal information include:
- Race and ethnic information.
- Sexual orientation.
- Political/religious beliefs.
- Social security or other taxpayer/government issued identification numbers.
- Financial information.
- National identification number or passport information.
- Health or medical information, including genetic information.
- Criminal records.
- And in some regions, such as the European Union, trade union membership.

*What Personal Information Do We Collect?*

We collect and maintain different types of personal information about you in accordance with applicable law. This includes the following:
- Name
- Gender
- Home address
- Telephone number
- Date of birth
- Marital status
- Employee identification number
- Emergency contacts
- Residency
- Work permit status
- Military status
- Nationality
- Passport information
- Social security or other taxpayer/government identification number
- Payroll information, banking details
- Wage and benefit information
- Retirement account information
- Sick pay, Paid Time Off, retirement accounts, pensions, insurance and other benefits information (including the gender, age, nationality and passport information for any spouse, minor children or other eligible dependents and beneficiaries).
- Information from interviews and phone-screenings you may have had, if any.
- Date of hire, date(s) of promotion(s), work history, technical skills, educational background, professional certifications and registrations, language capabilities, and training records.
- Beneficiary and emergency contact information.
- Forms and information relating to the application for, or in respect of changes to, employee health and welfare benefits; including, short and long-term disability, medical and dental care, etc.
- Physical limitations and special needs in order to provide [reasonable accommodations](/handbook/people-policies/inc-usa/#reasonable-accommodation).

- Records of work absences, vacation/paid time off, entitlement and requests, salary history and expectations, performance appraisals, letters of appreciation and commendation, and disciplinary and grievance procedures (including monitoring compliance with and enforcing our policies).

Where permitted by law and applicable we may collect the results of credit and criminal background checks, screening, health certifications, driving license number, vehicle registration, and driving history.

- Information required for us to comply with laws, the requests and directions of law enforcement authorities or court orders (e.g., child support and debt payment information).

- Acknowledgements regarding our policies, including employee handbooks, ethics and/or conflicts of interest policies, and computer and other corporate resource usage policies.

- Information captured on security systems and key card entry systems.

- Voicemails, e-mails, correspondence, documents, and other work product and communications created, stored or transmitted for professional or job related purposes using our networks, applications, devices, computers, or communications equipment.

 - Date of resignation or termination, reason for resignation or termination, information relating to administering termination of employment (e.g. references).

 - Letters of offer and acceptance of employment.

 - Your resume or CV, cover letter, previous and/or relevant work experience or other experience, education, transcripts, or other information you provide to us in support of an application and/or the application and recruitment process.

 - References and interview notes.

 - Information relating to any previous applications you may have made to GitLab and/or any previous employment history with GitLab.

For specifics about what information is collected by third party applications, please refer to the [Tech Stack Applications](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml).

Apart from personal information relating to yourself, you may also provide us with personal data of related parties, notably your dependents and other family members, for purposes of your HR administration and management, including the administration of benefits and to contact your next-of-kin in an emergency. Before you provide such third-party personal data to us you must first inform these third parties of any such data which you intend to provide and of the processing to be carried out by us. You must ensure and secure evidence that these related parties, or their legal representatives if they are minors, have given their free and express consent that their personal data may be processed by GitLab and/or its affiliates and subcontractors for the purposes described in this Privacy Policy.

*How is Data Collected?*

Generally, we collect personal information directly from you in circumstances where you provide personal information (during the onboarding process, for example). However, in some instances, the personal information we collect has been inferred about you based on other information you provide us, through your interactions with us, or from third parties. When we collect your personal information from third parties it is either because you have given us express consent to do so, your consent was implied by your actions (e.g., your use of a Third-Party employee service made available to you by us), or because you provided explicit consent to the Third-Party to provide the personal information to us.  Where permitted or required by applicable law or regulatory requirements, we may collect personal information about you without your knowledge or consent.

We reserve the right to monitor the use of our equipment, devices, computers, network, applications, software, and similar assets and resources for the safety and protection of employees and intellectual property. In the event such monitoring occurs, it may result in the collection of personal information about you.  If required by applicable law, we will notify you of such monitoring and obtain your consent.

*How We Process and Use Your Personal Information*

We may collect and process your personal information in the Systems for various purposes subject to local laws and any applicable collective bargaining agreements and works council agreements, including:

 - Recruitment, training, development, promotion, career, and succession planning

 - Appropriate vetting for recruitment and team allocation including, where relevant and appropriate, credit checks, right to work verification, identity fraud checks, relevant employment history, relevant regulatory status and professional qualifications

 - Providing and administering remuneration, salary, benefits, and incentive schemes and providing relevant information to payroll

 - Allocating and managing duties and responsibilities and the business activities to which they relate

 - Identifying and communicating effectively with other employees and management

 - Managing and operating conduct, performance, capability, absence, and grievance related reviews, allegations, complaints, investigations, and processes and other informal and formal HR processes and making related management decisions

 - Consultations or negotiations with representatives of the workforce

 - Conducting surveys for benchmarking and identifying improved ways of working employee relations and engagement at work (these will often be anonymous but may include profiling data such as age to support analysis of results)

 - Processing information about absence or medical information regarding physical or mental health or condition in order to assess eligibility for incapacity or permanent disability related remuneration or benefits, determine fitness for work, facilitate a return to work, make adjustments or [reasonable accommodations](/handbook/people-policies/inc-usa/#reasonable-accommodation) to duties or the workplace and make management decisions regarding employment or engagement or continued employment or engagement or redeployment and conduct related management processes

 - For planning, managing and carrying out restructuring or redundancies or other change programs including appropriate consultation, selection, alternative employment searches and related management decisions

 - Operating email, IT, Internet, intranet, social media, HR related and other company policies and procedures. The company carries out monitoring of GitLab's IT systems to protect and maintain the systems, to ensure compliance with GitLab policies and to locate information through searches where needed for a legitimate business purpose

 - Complying with applicable laws and regulation (for example maternity or parental leave legislation, working time and health and safety legislation, taxation rules, worker consultation requirements, other employment laws and regulation to which GitLab is subject in the conduct of its business)

 - Monitoring programs to ensure equality of opportunity and diversity with regard to personal characteristics protected under local anti-discrimination laws

 - Planning, due diligence and implementation in relation to a commercial transaction or service transfer involving GitLab that impacts on your relationship with GitLab (for example mergers and acquisitions or a transfer of your employment under automatic transfer rules)

 - For business operational and reporting documentation such as the preparation of annual reports or tenders for work or client team records including the use of your personal photo

 - In order to operate the relationship with Third-Party customer and suppliers including the disclosure of relevant vetting information in line with the appropriate requirements of regulated customers to those customers, contact or professional CV details or resume, or your personal photo for identification to clients or disclosure of information to data processors for the provision of services to GitLab

 - Where relevant for publishing appropriate internal or external communications or publicity material including via social media in appropriate circumstances, provided that privacy rights are preserved

 - To support HR administration and management and maintaining and processing general records necessary to manage the employment or worker relationship and operate the contract of employment or engagement

 - To centralize HR administration and management processing operations in an efficient manner for the benefit of our employees and to change access permissions

 - To provide support and maintenance for the System

 - To enforce our legal rights and obligations, and for any purposes in connection with any legal claims made by, against or otherwise involving you

 - To comply with lawful requests by public authorities (including without limitation to meet national security or law enforcement requirements), discovery requests, or where otherwise required or permitted by applicable laws, court orders, government regulations, or regulatory authorities (including without limitation data protection, tax and employment), whether within or outside your country

 - Other purposes permitted by applicable privacy and data protection legislation including where applicable, legitimate interests pursued by GitLab where this is not overridden by the interests or fundamental rights and freedoms of employees.


*Legal Basis for processing*

Where applicable data protection laws require us to process your personal data on the basis of a specific lawful justification, we generally process your personal data under one of the following bases:

Compliance with a legal obligation to which GitLab is subject; Entering into at-will employment (for US only) or performance under an employment contract with GitLab;
For GitLab's legitimate interests being those purposes described in the section above headed "How We Process and Use Your Personal Information";
Your consent where required and a legitimate legal basis under applicable local laws.

We may on occasion process your personal data for the purpose of the legitimate interests of a Third-Party where this is not overridden by your interests.

*Processing of Special Categories of Personal Data*

“Special Categories of Personal Data” includes information revealing racial or ethnic origin, political opinions, religious or philosophical beliefs, trade union membership, health, sex life or sexual orientation, as well as genetic and biometric data.

From time to time you may provide us with information which constitutes Special Categories of Personal Data or information from which Special Categories of Personal Data may be deduced.  In such cases, where required by law, we will obtain your express written consent to our processing of Special Categories of Personal Data.  If separate consent is not required by local law, by providing this information to GitLab, you give your freely given, informed, explicit consent for us to process those Special Categories of Personal Data for the purposes set out in How We Process and Use Your Personal Information section above.

You may withdraw your consent at any time by contacting GitLab's People Success Group or DPO. Where you have withdrawn consent but GitLab retains the personal data we will only continue to process that Special Category Personal Data where necessary for those purposes where we have another appropriate legal basis such as processing necessary to comply with legal obligations related to employment or social security. However, this may mean that we cannot (for example) administer certain benefits or contact your next-of-kin in an emergency or provide support to you above and beyond our legal obligations.  You give your knowledgeable, freely given, express consent to GitLab for GitLab to use, disclose and otherwise process any personal health information about you that is provided to GitLab by any of your personal health information custodians, for the purposes set out in the How We Process and Use Your Personal Information section above.

*Sharing Personal Information*

Your personal information may be shared, including to our affiliates, subsidiaries, and other third parties, as follows:

 - Where you request us or provide your consent to us.

 - In order to carry out the uses of personal information described above (see, How We Process and Use Your Personal Information).
When using or collaborating with third parties in the operation of our business, including in connection with providing many of the benefits and services we offer our employees (e.g., human resources information systems, financial investment service providers, insurance providers). When we share personal information with third parties we typically require that they only use or disclose such personal information in a manner consistent with the use and disclosure provisions of this Privacy Policy and applicable law.

 - We may buy or sell businesses and other assets. In such transactions, employee information is generally one of the transferred business assets and we reserve the right to include your personal information as an asset in any such transfer. Also, in the event that we, or substantially all of our assets, are acquired, your personal information may be one of the transferred assets.

 - Where required by law, by order or requirement of a court, administrative agency, or government tribunal, which includes in response to a lawful request by public authorities, including to meet national security or law enforcement requirements or in response to legal process.

 - If we determine it is necessary or desirable to comply with the law or to protect or defend our rights or property.

 - As necessary to protect the rights, privacy, safety, or property of an identifiable person or group or to detect, prevent or otherwise address fraud, security or technical issues, or to protect against harm to the rights, property or safety of GitLab, our users, applicants, candidates, employees or the public or as otherwise required by law.

 - Where the personal information is public and exempted from coverage under applicable data protection laws.

 - To seek advice from our lawyers and other professional advisors.

 - To professional advisors (e.g. bankers, lawyers, accountants) and potential buyers and vendors in connection with the sale, disposal or acquisition by use of a business or assets.

*Access to Personal Information We Collect*

To the extent access is required by applicable law, you can ask to see the personal information that we hold about you. If you want to review, verify or correct your personal information, please submit a request to GitLab's People Success Group or DPO.

When requesting access to your personal information, please note that we may request specific information from you to enable us to confirm your identity and right to access, as well as to search for and provide you with the personal information that we hold about you. We may, in limited circumstances, charge you a fee to access your personal information; however, we will advise you of any fee in advance.

We reserve the right not to grant access to personal information that we hold about you if access is not required by applicable law. There are also instances where applicable law or regulatory requirements allow or require us to refuse to provide some or all of the personal information that we hold about you. In addition, the personal information may have been destroyed, erased or made anonymous. In the event that we cannot provide you with access to your personal information, we will inform you of the reasons why, subject to any legal or regulatory restrictions.

*Correction of Collected Personal Information*

We endeavor to ensure that personal information in our possession is accurate, current and complete. If an individual believes that the personal information about him or her is incorrect, incomplete or outdated, he or she may request the revision or correction of that information. We reserve the right not to change any personal information we consider to be accurate or if such correction is not required by applicable law.

*Retention of Collected Information*

Except as otherwise permitted or required by applicable law or regulatory requirements, we may retain your personal information only for as long as we believe it is necessary to fulfill the purposes for which the personal information was collected (including, for the purpose of meeting any legal, accounting or other reporting requirements or obligations) and for IT archival purposes.

Personal data for data subjects in the European Union is by default erased by GitLab after termination of your employment, with the exception of certain types of personal data, which may be stored for an extended period of time due to administrative purposes, e.g. for payment of retirement income or for giving references to other employers, or where such personal data must be retained to comply with regulatory requirements.

You may request that we delete the personal information about you that we hold, provided that we reserve the right not to grant such request if we are not required to delete personal information under applicable law. There are instances where applicable law or regulatory requirements allow or require us to refuse to delete this personal information. In the event that we cannot delete your personal information, we will inform you of the reasons why, subject to any legal or regulatory restrictions.

*Requests to Access, Delete, or Correct Information*

Please send requests to access, delete, or correct your personal information to your DPO.

Any request by you to us to delete your personal information will not result in deletion of any information submitted by you to a Third-Party provider. If you require the Third-Party to delete any of your personal information, you must contact the Third-Party directly to request such deletion.

As stated previously, there are instances where applicable law or regulatory requirements allow or require us to refuse to delete this personal information. In the event that we cannot delete your personal information, we will inform you of the reasons why, subject to any legal or regulatory restrictions.

*Resolving Concerns*

If you have questions or concerns regarding the handling of your personal information, please contact GitLab's People Success Group or DPO. Alternatively, you may report concerns or complaints to the Legal Department.

You may also anonymously report violations of policy or law using our Third-Party managed Compliance & Fraud Prevention Hotline. You can access the Hotline by going to [How to Contact GitLab's 24 Hour Hotline](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#viii-questions-reporting-and-effect-of-violations)

*Changes to Privacy Policy*

We may change this Privacy Policy at any time by posting notice of such a change in the revision table below. The effective date of each version of this Privacy Policy is identified the revision table.

*Security of Collected Information*

We are committed to protecting the security of the personal information collected, and we take reasonable physical, electronic, and administrative safeguards to help protect the information from unauthorized or inappropriate access or use.

**Additional Rights**
You may also have the following additional rights, subject to certain exceptions and limitations as specified in applicable law:

*Data portability*

Where we are relying upon your consent or the fact that the processing is necessary for the performance of a contract to which you are party as the legal basis for processing, and that personal information is processed by automatic means, to the extent provided under applicable law, you have the right to receive all such personal information which you have provided to GitLab in a structured, commonly used and machine-readable format, and also to require us to transmit it to another controller where this is technically feasible;

*Right to restriction of processing*

You have the right to restrict our processing of your personal information where:

 - You contest the accuracy of the personal information until we have taken sufficient steps to correct or verify its accuracy;
 - Where the processing is unlawful, but you do not want us to erase the information;
 - Where we no longer need the personal information for the purposes of the processing, but you require them for the establishment, exercise or defense of legal claims; or
 - Where you have objected to processing justified on legitimate interest grounds (see below) pending verification as to whether GitLab has compelling legitimate grounds to continue processing.

To the extent required by applicable law, where personal information is subjected to restriction in this way we will only process it with your consent or for the establishment, exercise or defense of legal claims.

*Right to withdraw consent*

Where we are relying upon your consent to process data, you have the right to withdraw such consent at any time. You can do this by contacting GitLab's People Success Group or DPO.

*Right to object to processing justified on legitimate interest grounds*

Where we are relying upon legitimate interest to process data, then you have the right to object to such processing, and we must stop such processing unless we can either demonstrate compelling legitimate grounds for the processing that override your interests, rights and freedoms or where we need to process the data for the establishment, exercise or defense of legal claims. Normally, where we rely upon legitimate interest as a basis for processing we believe that we can demonstrate such compelling legitimate grounds, but we will consider each case on an individual basis.

You also have the right to lodge a complaint with a supervisory authority, in particular in your country of residence, if you consider that the processing of your personal data infringes this regulation.

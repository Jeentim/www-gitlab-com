---
layout: markdown_page
title: "Category Direction - Live Preview"
description: "With Live Preview can view your simple JavaScript apps and static sites in the Web IDE, in real time, right next to the code."
canonical_path: "/direction/create/editor/live_preview/"
---

- TOC
{:toc}

## Live Preview

| | |
| --- | --- |
| Stage | [Create](/direction/dev/#create) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2021-10-29` |

### Introduction and how you can help

Thanks for visiting this direction page for the [Live Preview](https://docs.gitlab.com/ee/user/project/web_ide/index.html#enabling-client-side-evaluation) feature of the [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/index.html). This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by Eric Schurter ([E-Mail](mailto:eschurter@gitlab.com)). More information about the Editor group's priorities and direction can be found on the [Editor group direction page](/direction/create/editor/).

Learn more about the [Web IDE direction](/direction/create/editor/web_ide/).

### Overview

Developers working on projects often need to preview their changes while developing, during review and to demo progress. Previewing changes often involves having pre-configured development environments running locally or in the cloud.

### Where we are headed 

With Live Preview in the Web IDE you can view your simple JavaScript apps and static sites, in real time, right next to the code. We're not [currently investing in the Live Preview features](/direction/create/editor/) of the Web IDE, but we welcome contributions to improve the existing feature or extend our current Live Preview capabilities.

GitLab is also supportive of 3rd party integrations to extend these features to developers. One example is the community contribution [Gitpod](https://www.gitpod.io/) made to add a native integration to GitLab, making it possible to [launch a remote Gitpod instance](https://docs.gitlab.com/ee/integration/gitpod.html) from the repository or merge request with a single click.

### What's Next & Why

While we aren't actively investing in specific functionality for Live Preview at the moment, we continue to monitor and consider new opportunities that may address the core jobs to be done.

**In Progress:** Investigate using Service Workers for client-side Live Preview [#218438](https://gitlab.com/gitlab-org/gitlab/-/issues/218438)

In order to support binary and static asset files in Live Preview in the Web IDE an investigation is underway with Codesandbox to use Service Workers to load these files. Current findings summary is in this [comment](https://gitlab.com/gitlab-org/gitlab/-/issues/218438#note_378545011).

**Opportunites for additional integrations**

Okteto Integration in Web IDE [&4139](https://gitlab.com/groups/gitlab-org/-/epics/4139)

Currently [Okteto](https://okteto.com/) is interested in enabling their service through the Web Terminal available in the Web IDE. Discovery is underway to see what blockers exist to enabling this and enhancements that may be required.

### What is Not Planned Right Now

Live Preview is currently limited to Javascript applications that can be evaluated in the browser, called client-side evaluation. We are not actively working on extending support to include server-side evaluation which would allow more complex applications to be previewed in real time through the Web IDE but would have increasingly complex and costly infrastructure requirements. 
